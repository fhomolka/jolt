package jolt

import "core:c"
import m "core:math/linalg/hlsl"

//foreign import Jolt "build/jolt_bind.lib"
foreign import Jolt{
    "system:Kernel32.lib",
    "system:Gdi32.lib",
    "build/jolt_bind.lib",
}

//opaque types
TempAllocatorImpl :: struct{}
JobSystem :: struct{}
JPC_BodyInterface :: struct{}
JPC_CharacterVirtual :: struct{}
JPC_Shape :: struct{}
JPC_GroupFilter :: struct{}
JPC_CharacterContactSettings :: struct{}
JPC_PhysicsMaterial :: struct{}
JPC_PhysicsSystem :: struct{}
JPC_BoxShapeSettings :: struct{}
JPC_ShapeSettings :: struct{}
JPC_ConvexShapeSettings :: struct{}         
JPC_SphereShapeSettings:: struct{} 
JPC_TriangleShapeSettings:: struct{}
JPC_CapsuleShapeSettings::struct{}
JPC_TaperedCapsuleShapeSettings::struct{}
JPC_CylinderShapeSettings::struct{}
JPC_ConvexHullShapeSettings::struct{}
JPC_HeightFieldShapeSettings::struct{}
JPC_MeshShapeSettings::struct{}
JPC_DecoratedShapeSettings::struct{}
JPC_CompoundShapeSettings::struct{}

ObjectLayer :: distinct c.uint16_t
BroadPhaseLayer :: distinct c.uint8_t
JPC_BodyID :: distinct c.uint32_t
JPC_CollisionGroupID :: distinct c.uint32_t
JPC_CollisionSubGroupID :: distinct c.uint32_t
JPC_SubShapeID :: distinct c.uint32_t

JPC_MotionQuality :: enum c.uint8_t{
    JPC_MOTION_QUALITY_DISCRETE    = 0,
    JPC_MOTION_QUALITY_LINEAR_CAST = 1
}

JPC_MotionType :: enum c.uint8_t{
    JPC_MOTION_TYPE_STATIC    = 0,
    JPC_MOTION_TYPE_KINEMATIC = 1,
    JPC_MOTION_TYPE_DYNAMIC   = 2
}

// NOTE: Needs to be kept in sync with JPH::ContactSettings
JPC_ContactSettings :: struct
{
	combined_friction : f32,
	combined_restitution : f32,
	is_sensor : c.bool,
} 


// NOTE: Needs to be kept in sync with JPH::ContactManifold
JPC_ContactManifold :: struct
{
	base_offset : m.float4,
	normal : m.float4,
	penetration_depth : f32,
	shape1_sub_shape_id : JPC_SubShapeID,
	shape2_sub_shape_id : JPC_SubShapeID,

	//nested struct
	shape1_relative_contact : struct{
		num_points : c.uint32_t,//alignas16
		dummy : [3]c.uint32_t,

		points : [32][4]c.float,//align as 16
	},
	shape2_relative_contact : struct{
		num_point : c.uint32_t,
		dummy : [3]c.uint32_t,//aligning as 16
		points : [32][4]c.float,//aslign as 16//should be 512 bytes which is on 16 bytes boundary
	}

	/*
    JPC_RVEC_ALIGN JPC_Real  base_offset[4]; // 4th element is ignored
    alignas(16) float        normal[4]; // 4th element is ignored; world space
    float                    penetration_depth;
    JPC_SubShapeID           shape1_sub_shape_id;
    JPC_SubShapeID           shape2_sub_shape_id;
    struct {
        alignas(16) uint32_t num_points;
        alignas(16) float    points[64][4]; // 4th element is ignored; world space
    }                        shape1_relative_contact;
    struct {
        alignas(16) uint32_t num_points;
        alignas(16) float    points[64][4]; // 4th element is ignored; world space
    }                        shape2_relative_contact;
*/
}

// NOTE: Needs to be kept in sync with JPH::CollideShapeResult
JPC_CollideShapeResult :: struct
{
	shape_1_contact_point : m.float4,
	shape_2_contact_point : m.float4,
	penetration_axis : m.float4,
	depth : f32,
    shape1_sub_shape_id : JPC_SubShapeID,
    shape2_sub_shape_id : JPC_SubShapeID,
	body2 : JPC_BodyID,


	//nested struct
	shape1_face : struct{
		num_points : c.uint32_t,//alignas16
		dummy : [3]c.uint32_t,

		points : [32][4]c.float,//align as 16
	},
	shape2_face : struct{
		num_point : c.uint32_t,
		dummy : [3]c.uint32_t,//aligning as 16
		points : [32][4]c.float,//aslign as 16//should be 512 bytes which is on 16 bytes boundary
	}

	/*
    alignas(16) float        shape1_contact_point[4]; // 4th element is ignored; world space
    alignas(16) float        shape2_contact_point[4]; // 4th element is ignored; world space
    alignas(16) float        penetration_axis[4]; // 4th element is ignored; world space
    float                    penetration_depth;
    JPC_SubShapeID           shape1_sub_shape_id;
    JPC_SubShapeID           shape2_sub_shape_id;
    JPC_BodyID               body2_id;
    struct {
        alignas(16) uint32_t num_points;
        alignas(16) float    points[32][4]; // 4th element is ignored; world space
    }                        shape1_face;
    struct {
        alignas(16) uint32_t num_points;
        alignas(16) float    points[32][4]; // 4th element is ignored; world space
    }                        shape2_face;
	*/
}


// NOTE: Needs to be kept in sync with JPH::MotionProperties
JPC_MotionProperties :: struct{
	//align as 16!!!
	linear_velocity : [4]c.float,
	angular_velocity : [4]c.float,
	inv_inertia_diagonal : [4]c.float,
	inertia_rotation : [4]c.float,

	force : m.float3,
	torque : m.float3,
	inv_mass : c.float,
	linear_damping : c.float,
	angular_damping : c.float,
	max_linear_velocity : c.float,
	max_angular_velocity : c.float,
	gravity_factor : c.float,
	index_in_active_bodies : c.uint32_t,
	island_index : c.uint32_t,
	motion_quality : JPC_MotionQuality,
	allow_sleeping : c.bool,

	reserved : [52]c.uint8_t,
	//if enabled asserts assumed off
	//cached_motion_type : JPC_MotionType
	/*
    alignas(16) float  linear_velocity[4]; // 4th element is ignored
    alignas(16) float  angular_velocity[4]; // 4th element is ignored
    alignas(16) float  inv_inertia_diagonal[4]; // 4th element is ignored
    alignas(16) float  inertia_rotation[4];

    float              force[3];
    float              torque[3];
    float              inv_mass;
    float              linear_damping;
    float              angular_damping;
    float              max_linear_velocity;
    float              max_angular_velocity;
    float              gravity_factor;
    uint32_t           index_in_active_bodies;
    uint32_t           island_index;

    JPC_MotionQuality  motion_quality;
    bool               allow_sleeping;

#if JPC_DOUBLE_PRECISION == 1
    alignas(8) uint8_t reserved[76];
#else
    alignas(4) uint8_t reserved[52];
#endif

#if JPC_ENABLE_ASSERTS == 1
    JPC_MotionType     cached_motion_type;
#endif
*/
}

// NOTE: Needs to be kept in sync with JPH::CollisionGroup
JPC_CollisionGroup :: struct{
	filter : ^JPC_GroupFilter,
	group_id : JPC_CollisionGroupID,
	sub_group_id : JPC_CollisionSubGroupID,
	/*
    const JPC_GroupFilter * filter;
    JPC_CollisionGroupID    group_id;
    JPC_CollisionSubGroupID sub_group_id;
	*/
}
// NOTE: Needs to be kept in sync with JPH::CollisionGroup
//Need a way to also handle double precision for now we are only dealing with single precision
JPC_Body :: struct{
	//align as 16
	position : m.float4,
	rotation : m.float4,
	bounds_min : m.float4,
	bounds_max : m.float4,
	//end align
	shape : ^JPC_Shape,
	motion_properties : ^JPC_MotionProperties,
	user_data : u64,
	collision_group : JPC_CollisionGroup,
	friction : f32,
	restitution : f32,
	id : JPC_BodyID,
	object_layer : ObjectLayer,
	broad_phase_layer : BroadPhaseLayer,
	motion  : JPC_MotionType,

	/*
    JPC_RVEC_ALIGN JPC_Real position[4]; // 4th element is ignored
    alignas(16) float       rotation[4];
    alignas(16) float       bounds_min[4]; // 4th element is ignored
    alignas(16) float       bounds_max[4]; // 4th element is ignored

    const JPC_Shape *       shape;
    JPC_MotionProperties *  motion_properties; // will be NULL for static bodies
    uint64_t                user_data;
    JPC_CollisionGroup      collision_group;

    float                   friction;
    float                   restitution;
    JPC_BodyID              id;

    JOLT_ObjectLayer         object_layer;

    JOLT_BroadPhaseLayer     broad_phase_layer;
    JPC_MotionType          motion_type;
    uint8_t                 flags;
	*/

}
//Physics settings

/// Maximum amount of jobs to allow
cMaxPhysicsJobs : u32 = 2048;

/// Maximum amount of barriers to allow
cMaxPhysicsBarriers : u32 = 8;

BroadPhaseLayerInterfaceVTable :: struct{
	//What goe 
	dummy : c.uint64_t,
	GetNumBroadPhaseLayers : proc "c" ()->c.uint32_t,
	GetBroadPhaseLayer : proc "c" (out_layer : BroadPhaseLayer,in_layer : ObjectLayer),
	//if profile enabled can get name
	//TODO
}

ObjectLayerPairFilterVTable :: struct{
	dummy : rawptr,
	ShouldCollide : proc "c" (in_self : rawptr,in_layer1 : ObjectLayer, in_layer2 : ObjectLayer)->bool
}

ObjectLayerVsBroadPhaseLayerFilter :: struct{
	dummy : rawptr,
	ShouldCollide : proc "c" (in_self : rawptr,in_layer1 : ObjectLayer, in_layer2 : BroadPhaseLayer)->bool
}

CharacterContactListener :: struct{
	dummy : rawptr,
	//validate result enum not dummy
	OnAdjustBodyVelocity : proc "c"(in_self : rawptr,in_character : ^JPC_CharacterVirtual,in_body2 : ^JPC_Body,io_linear_velocity : m.float3,io_angular_velocity : m.float3),
	OnContactValidate : proc "c"(in_self : rawptr,in_character : ^JPC_CharacterVirtual,in_body2 : ^JPC_Body,sub_shape_id : ^JPC_SubShapeID),
	OnContactAdded : proc "c" (in_self : rawptr,in_character : ^JPC_CharacterVirtual,in_body2 : ^JPC_Body,sub_shape_id : ^JPC_SubShapeID,contact_position : m.float3,contact_normal : m.float3,io_settings : ^JPC_CharacterContactSettings),
	OnContactSolve : proc "c" (in_self : rawptr,in_character : ^JPC_CharacterVirtual,in_body2 : ^JPC_Body,sub_shape_id : ^JPC_SubShapeID,contact_position : m.float3,contact_normal : m.float3, contact_velocity : m.float3,contact_material : ^JPC_PhysicsMaterial,character_velocity_in : m.float3,character_velocity_out : m.float3),
}

JPC_ValidateResult :: enum c.int
{
    JPC_VALIDATE_RESULT_ACCEPT_ALL_CONTACTS = 0,
    JPC_VALIDATE_RESULT_ACCEPT_CONTACT      = 1,
    JPC_VALIDATE_RESULT_REJECT_CONTACT      = 2,
    JPC_VALIDATE_RESULT_REJECT_ALL_CONTACTS = 3,
    _JPC_VALIDATE_RESULT_FORCEU32           = 0x7fffffff
}

ContactListener :: struct{
	//dummy : rawptr,
	OnContactValidate : proc "c"(in_body : ^JPC_Body,in_body2 : ^JPC_Body,in_base_offset : m.float3,in_collision_result : ^JPC_CollideShapeResult) -> JPC_ValidateResult, 
	OnContactAdded : proc "c"(in_body : ^JPC_Body,in_body2 : ^JPC_Body,in_manifold : ^JPC_ContactManifold,io_settings : ^JPC_ContactSettings)
}

@(default_calling_convention="c")
@(link_prefix = "JOLT_")
foreign Jolt
{
	dummy_func :: proc "c" () -> c.int ---
	RegisterDefaultAllocator :: proc "c" () ---
	RegisterTypes :: proc "c" () ---
	TempAllocator_Create :: proc "c"( in_size : u32)-> ^TempAllocatorImpl ---
 	TempAllocator_Destroy :: proc "c"(in_allocator : ^TempAllocatorImpl) ---
	JobSystem_Create :: proc "c"(in_max_jobs : u32,in_max_barriers : u32,in_num_threads : c.int) -> ^JobSystem --- 
	JobSystem_Destroy :: proc "c"(in_job_system : ^JobSystem) ---
	PhysicsSystem_Create :: proc "c"(in_max_bodies : c.uint32_t,in_num_body_mutexes : c.uint32_t,in_max_body_pairs : c.uint32_t,in_max_constraints : c.uint32_t,in_broad_phase_layer_interface : BroadPhaseLayerInterfaceVTable,in_object_vs_broad_phase_layer_filter : rawptr,in_object_layer_pair_filter : rawptr) -> ^JPC_PhysicsSystem---
	SetContactListener :: proc "c"(ps : ^JPC_PhysicsSystem,listener : ^ContactListener) ---
	GetBodyInterface :: proc "c"(ps : ^JPC_PhysicsSystem) -> ^JPC_BodyInterface ---
	BoxShapeSettings_Create :: proc "c"(in_half_extent : m.float3) -> ^JPC_BoxShapeSettings---
	BoxShapeSettings_GetHalfExtent :: proc "c" (in_settings : ^JPC_BoxShapeSettings,out_half_extent : ^m.float3)---
	BoxShapeSettings_SetHalfExtent :: proc "c" (in_settings : ^JPC_BoxShapeSettings,in_half_extent : ^m.float3) --- 
	BoxShapeSettings_GetConvexRadius :: proc "c" (in_settins : ^JPC_BoxShapeSettings) -> c.float ---
	BoxShapeSettings_SetConvexRadius :: proc "c" (in_settings : ^JPC_BoxShapeSettings,in_convex_radius : ^m.float3) ---
	ShapeSettings_AddRef :: proc "c" (in_settings : ^JPC_ShapeSettings)---
	ShapeSettings_Release :: proc "c" (in_settings : ^JPC_ShapeSettings) ---
	ShapeSettings_GetRefCount :: proc "c" (in_settings : ^JPC_ShapeSettings ) -> c.int32_t ---
/// First call creates the shape, subsequent calls return the same pointer and increments reference count.
/// Call `JPC_Shape_Release()` when you don't need returned pointer anymore.
	ShapeSettings_CreateShape :: proc "c" (in_settings: ^JPC_ShapeSettings) -> ^JPC_Shape --- 
	ShapeSettings_GetUserData :: proc "c" (in_settings : ^JPC_ShapeSettings ) -> c.uint64_t ---
	ShapeSettings_SetUserData :: proc "c" (in_settings : ^JPC_ShapeSettings ,in_user_data : c.uint64_t) --- 
}

