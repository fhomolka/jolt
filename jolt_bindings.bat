REM call "C:\Program Files (x86)\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvarsall.bat" x64
@echo off

setlocal

where cl >nul 2>nul
IF %ERRORLEVEL% NEQ 0 (echo WARNING: cl is not in the path - please set up Visual Studio to do cl builds)

IF NOT EXIST build mkdir build
pushd build

REM set CommonCompilerFlagsNoLink= -std:c++17 -EHsc -LD -MTd -nologo -fp:fast -Gm- -GR- -sdl- -EHa- -Od -Oi -WX -W4 -wd4457 -wd4018 -wd4459 -wd4389 -wd4312 -wd4245 -wd4996 -wd4201 -wd4100 -wd4506 -wd4127 -wd4189 -wd4505 -wd4577 -wd4101 -wd4702 -wd4456 -wd4238 -wd4244 -wd4366 -wd4700 -wd4701 -wd4703 -wd4805 -wd4091 -wd4706 -wd4197 -wd4324 -FC -ZI 
set CommonCompilerFlagsNoLink= -std:c++17 -EHsc -c -MT -nologo -fp:fast -Gm- -GR- -sdl- -EHa- -Od -Oi -WX -W4 -wd4457 -wd4018 -wd4459 -wd4389 -wd4312 -wd4245 -wd4996 -wd4201 -wd4100 -wd4506 -wd4127 -wd4189 -wd4505 -wd4577 -wd4101 -wd4702 -wd4456 -wd4238 -wd4244 -wd4366 -wd4700 -wd4701 -wd4703 -wd4805 -wd4091 -wd4706 -wd4197 -wd4324 -FC -ZI -DJPH_PROFILE_ENABLED -DJPH_DEBUG_RENDERER -DJPH_FLOATING_POINT_EXCEPTIONS_ENABLED -DJPH_USE_AVX2 -DJPH_USE_AVX -DJPH_USE_SSE4_1 -DJPH_USE_SSE4_2 -DJPH_USE_LZCNT -DJPH_USE_TZCNT -DJPH_USE_F16C -DJPH_USE_FMADD 
REM set CommonLinkerFlags= /LIBPATH:"../" /NODEFAULTLIB:libcmt.lib  -opt:noref jolt.lib
set CommonLinkerFlags= /NODEFAULTLIB:libcmtd.lib  
cl %CommonCompilerFlagsNoLink% /I"../JoltPhysics" ../jolt_bind.cpp -Fdjolt_bind.pdb -Fmjolt_bind.map 
REM /link %CommonLinkerFlags%
REM lib /OUT:jolt_bind.lib jolt_bind.obj ../jolt.lib
lib  /OUT:jolt_bind.lib jolt.lib jolt_bind.obj 

REM set CommonCompilerFlags= -std:c++14 -EHsc -arch:AVX -GR- -MTd -nologo -fp:fast -Gm- -GR- -sdl- -EHa- -Od -Oi -WX -W4 -wd4457 -wd4018 -wd4459 -wd4389 -wd4312 -wd4245 -wd4996 -wd4201 -wd4100 -wd4506 -wd4127 -wd4189 -wd4505 -wd4577 -wd4101 -wd4702 -wd4456 -wd4238 -wd4244 -wd4366 -wd4700 -wd4701 -wd4703 -wd4805 -wd4091 -wd4706 -wd4197 -wd4324 -FC -ZI
REM set CommonLinkerFlags=  /LIBPATH:"../../build" /NODEFAULTLIB:libcmt.lib  -opt:noref Kernel32.lib user32.lib gdi32.lib winmm.lib Ws2_32.lib Ole32.lib Xinput9_1_0.lib DXGI.lib D3D12.lib D3DCompiler.lib fmj.lib

REM cl %CommonCompilerFlags%  ../main.cpp -Fmmain.map /link -PDB:main.pdb %CommonLinkerFlags%
REM popd
popd
